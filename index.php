<?php 
require_once 'Logica/edition.php';
require_once 'Logica/editiontopic.php';
require_once 'Logica/topic.php';
require 'persistencia/conexion.php';


$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);
}
?>
<head>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="icon" type="image/jpg" href="img/imagen.jpg" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

	<script>

	$(function () {

		$('[data-toggle="tooltip"]').tooltip('hide');

	})

	</script>
	
</head>
<?php 
include "Presentacion/encabezado.php";
if($pid!=""){
    include $pid;
}

?>
