<?php
require "persistencia/EditiontopicDAO.php";

class Editiontopic{

    private $idEditiontopic;
    private $accepted;
    private $rejected;
    //private $edition_idEdition;
    // private $topic_idTopic;


    
    /**
     * @return mixed
     */
    public function getidEditiontopic()
    {
        return $this->idEditiontopic;
    }

    /**
     * @return mixed
     */
    public function getaccepted()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getrejected()
    {
        return $this->year;
    }


    function Editiontopic ($pIdEditiontopic="", $pAccepted="", $pRejected="") {

        $this -> idEditiontopic = $pIdEditiontopic;
        $this -> accepted = $pAccepted;
        $this -> rejected = $pRejected;
        $this -> conexion = new Conexion();
        $this -> EditiontopicDAO = new EditiontopicDAO($pIdEditiontopic, $pAccepted, $pRejected);        
    }


    
  /*  function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> libroDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $libros = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $autor = new Autor($resultado[5]);
            $autor -> consultar();            
            array_push($libros, new Libro($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $autor));
        }
        return $libros;
    } */
    
    function consultarPapelesaceptados($year){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> EditiontopicDAO -> consultarPapelesaceptados($year));
        $this -> conexion -> cerrar();
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){

             array_push($resultados, array($resultado[0],$resultado[1],$resultado[2]));
         //   array_push($resultados, array($resultado[0],$resultado[1],$resultado[2],$resultado[3],$resultado[4],$resultado[5]
           //     ,$resultado[6]);
        }
        return $resultados;
    }
}


?>