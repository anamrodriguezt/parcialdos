<?php
require "persistencia/EditionDAO.php";

class Edition{

    private $idEdition;
    private $name;
    private $year;
    private $startDate;
    private $endDate;
    private $internationalCollaboration;
    private $numberOfKeynotes;

    
    /**
     * @return mixed
     */
    public function getidEdition()
    {
        return $this->idEdition;
    }

    /**
     * @return mixed
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getyear()
    {
        return $this->year;
    }

    /**
     * @return mixed
     */
    public function getstartDate()
    {
        return $this->startDate;
    }

    /**
     * @return mixed
     */
    public function getendDate()
    {
        return $this->endDate;
    }

    /**
     * @return mixed
     */
    public function getinternationalCollaboration()
    {
        return $this->internationalCollaboration;
    }


       public function getnumberOfKeynotes()
    {
        return $this->numberOfKeynotes;
    }


    function Edition ($pIdEdition="", $pName="", $pYear="", $pStartDate="", $pEndDate="", $pInternationalCollaboration="", 
        $pNumberOfKeynotes="") {

        $this -> idEdition = $pIdEdition;
        $this -> name = $pName;
        $this -> year = $pYear;
        $this -> startDate = $pStartDate;
        $this -> endDate = $pEndDate;
        $this -> internationalCollaboration = $pInternationalCollaboration;
        $this -> numberOfKeynotes = $pNumberOfKeynotes;
        $this -> conexion = new Conexion();
        $this -> EditionDAO = new EditionDAO($pIdEdition, $pName, $pYear, $pStartDate, $pEndDate, $pInternationalCollaboration,
            $pNumberOfKeynotes);        
    }


    
  /*  function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> libroDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $libros = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $autor = new Autor($resultado[5]);
            $autor -> consultar();            
            array_push($libros, new Libro($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $autor));
        }
        return $libros;
    } */
    
    function consultarPapelesaceptados(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> EditionDAO -> consultarPapelesaceptados());
        $this -> conexion -> cerrar();
        $resultados = array();
        while(($resultado = $this -> conexion -> extraer()) != null){

             array_push($resultados, array($resultado[0],$resultado[1],$resultado[2]));
         //   array_push($resultados, array($resultado[0],$resultado[1],$resultado[2],$resultado[3],$resultado[4],$resultado[5]
           //     ,$resultado[6]);
        }
        return $resultados;
    }
}


?>