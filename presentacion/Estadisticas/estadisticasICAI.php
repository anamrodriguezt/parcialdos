<div class="container mt-4">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-12">             
<div>
    <form action=<?php echo "index.php?pid=" . base64_encode("presentacion/Estadisticas/estadisticasICAI.php") ?>
                        method="post">
     <select name="year"> 
        <option value="2020">2020</option>
        <option value="2019">2019</option>
        <option value="2018">2018</option>
    </select>   
    <input type="submit" value="MIRAR AÑOS" name="Buscar">
    </form>
</div>
<?php

$editions= [];

if (isset($_POST["year"])) {
 $editiontopic = new Editiontopic();
$editions= $editiontopic->consultarPapelesaceptados($_POST["year"]);    
}

?>
			 	<div class="card-body">
                    Papeles ICAI
              		<script type="text/javascript">
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                      var data = google.visualization.arrayToDataTable([
                        ['Task', 'Hours per Day'],
                    	<?php 
                    			$aceptados = 0;
                    			$noaceptados = 0;
                    			foreach($editions as $p){
                    			   $aceptados += $p["0"];
                                   $noaceptados += $p["1"];
                    			}
                    		    ?>
                    		    ['aceptados', <?php echo $aceptados?>],
                    		    [' No aceptados', <?php echo $noaceptados?>]
                    		    //     ['Eat',      2],
                    		    //     ['Commute',  2],
                    		    //     ['Watch TV', 2],
                    		    //     ['Sleep',    7]
                    			  ]);
                      var options = {
                        title: 'Papeles aceptados'
                      };
                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                      chart.draw(data, options);
                    }
                    </script>
        			<div id="piechart" style="width: 900px; height: 500px;"></div>
    			</div>
    		</div>
    	</div>
    </div>