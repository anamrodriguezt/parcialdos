<?php
class EditionDAO{

    

    private $idEdition;
    private $name;
    private $year;
    private $startDate;
    private $endDate;
    private $internationalCollaboration;
    private $numberOfKeynotes;

 /**
     * @return mixed
     */
    public function getidEdition()
    {
        return $this->idEdition;
    }

    /**
     * @return mixed
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getyear()
    {
        return $this->year;
    }

    /**
     * @return mixed
     */
    public function getstartDate()
    {
        return $this->startDate;
    }

    /**
     * @return mixed
     */
    public function getendDate()
    {
        return $this->endDate;
    }

    /**
     * @return mixed
     */
    public function getinternationalCollaboration()
    {
        return $this->internationalCollaboration;
    }


       public function getnumberOfKeynotes()
    {
        return $this->numberOfKeynotes;
    }


    function EditionDAO ($pIdEdition="", $pName="", $pYear="", $pStartDate="", $pEndDate="", $pInternationalCollaboration="", 
        $pNumberOfKeynotes="") {

        $this -> idEdition = $pIdEdition;
        $this -> name = $pName;
        $this -> year = $pYear;
        $this -> startDate = $pStartDate;
        $this -> endDate = $pEndDate;
        $this -> internationalCollaboration = $pInternationalCollaboration;
        $this -> numberOfKeynotes = $pNumberOfKeynotes;      
    }


/*
    
    function consultarTodos () {
        return "select idLibro, titulo, paginas, precio, descripcion, Autor_idAutor
                from Libro";
    }

*/
   // function consultarPapelesaceptados () {
    /*    return "select a.nombre, a.apellido, count(l.idLibro) as total
                from Autor a join Libro l on (a.idAutor = l.Autor_idAutor)
                group by a.nombre, a.apellido
                order by total desc";
    //} */
    

}


?>